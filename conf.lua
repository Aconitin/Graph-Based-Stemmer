function love.conf(t) --> Engine configuration file! Don't do anything here!
    t.title = "GRAS Implementation using LUA as JIT & LOEVE Engine, by Sven Raveny"
    t.author = "Sven Raveny"
    t.url = "svenraveny.net"
    t.identity = "SvenRaveny GRAS"
    t.version = "0.8.0"
    t.console = true
    t.release = true
	t.screen = false
    t.screen.width = 960
    t.screen.height = 540
    t.screen.fullscreen = false
    t.screen.vsync = true
    t.screen.fsaa = 0
    t.modules.joystick = false
    t.modules.audio = false
    t.modules.keyboard = true
    t.modules.event = true
    t.modules.image = true
    t.modules.graphics = true
    t.modules.timer = true
    t.modules.mouse = true
    t.modules.sound = false
    t.modules.physics = false
end